
package domain;

public class Persona {
    protected String nombre;  //el uso de la palabra protected esta pensado para las clases padres para que sus hijas puedan
    protected char genero;                //acceder a dichos atributos, no como public o private que tienen sus limitantes.
    protected int edad;
    protected String direccion;
    
    // Los Contructores, de la clase padre no se heredan, por lo que se usa la palabra "super(parametros)" para utilizar alguno.
    public Persona(){
        
    }
    
    public Persona(String nombre){
        this.nombre=nombre;   
    }

    public Persona(String nombre, char genero, int edad, String direccion) {
        this.nombre = nombre;
        this.genero = genero;
        this.edad = edad;
        this.direccion = direccion;
    }

    //Get and Set para cada atributo
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getGenero() {
        return this.genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public int getEdad() {
        return this.edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    //Agregamos toString
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Persona{nombre=").append(nombre);
        sb.append(", genero=").append(genero);
        sb.append(", edad=").append(edad);
        sb.append(", direccion=").append(direccion);
        sb.append(", ").append(super.toString());// en esta linea usamos el toString de la clase Object
        sb.append('}');                         // para mostrar la direccion de memoria del la Persona.
        return sb.toString();
    }
    
       
} //Fin class Persona