
package domain;

public class Empleado extends Persona{ //al usar "extends" decimos que sera una extencion de la clase Persona
    private int idEmpleado;           // para poder acceder a los atributos y metodos de la class padre Persona.
    private double sueldo;
    private static int contadorEmpleado; //static se usa para que el valor del contador se mantenga guardado en la clase.

    //Constructores
    public Empleado(){
        //super(); aunque sea un constructor vacio, de manera implicita llama al constructor de la clase padre por medio de super
        this.idEmpleado= ++Empleado.contadorEmpleado;
    } 
    
    public Empleado(String nombre,double sueldo) {
        this(); // es el equivalente a llamar al constructor vacio.
        this.nombre=nombre; //utilizamos el atributo de la class padre ya que son de tipo "protected".
        this.sueldo = sueldo;
    }

    // Get and Set
    public int getIdEmpleado() { // no ponemos el setEmpleado, ya que no queremos modificar idEmpleado manualmente.
        return this.idEmpleado;
    }

    public double getSueldo() {
        return this.sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }
    
    //Metodo toString 
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(); //Con la class StringBuilder creamos un objeto cadena
        sb.append("Empleado{idEmpleado=").append(this.idEmpleado); //utilizando el metodo "append" vamos concatenando, cargando la 
        sb.append(", sueldo=").append(this.sueldo); // informacion, es una forma de agregar la informacion pero de manera mas eficiente.
        sb.append(", ").append(super.toString());   //utilizamos mediante super, el metodo toString de su class padre Persona
        sb.append('}');                             //para lograr mostrar los datos completos de dicho empleado (una persona)).
        return sb.toString(); //al finalizar mandamos la variable "sb", pero con el metodo "toString" que regresa un tipo String.
    }
     
    
    
    
    
    
    
    
} //Fin class Empleado
