
package domain;

import java.util.Date;

public class Cliente extends Persona { // es una extencion de Persona
    private int idCliente;
    private Date fechaRegistro; // le vamos a asignar un objeto de tipo dato
    private boolean vip;
    private static int contadorCliente; // es static para mantener su valor en la class
    
    //Contructor
    public Cliente(Date fechaRegistro,boolean vip, String nombre, char genero, int edad, String direccion) {
        super(nombre,genero,edad,direccion);
        this.idCliente= ++Cliente.contadorCliente;
        this.fechaRegistro= fechaRegistro;
        this.vip = vip;
    }
    
    //get and set
    public int getIdCliente() { // sin el "setIdCliente" para no modificarlo manualmente.
        return this.idCliente;
    }

    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public boolean isVip() {
        return this.vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    //toString
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cliente{idCliente=").append(this.idCliente);
        sb.append(", fechaRegistro=").append(this.fechaRegistro);
        sb.append(", vip=").append(this.vip);
        sb.append(", ").append(super.toString()); //mostrar los atributos de la persona
        sb.append('}');
        return sb.toString();
    }
   
} //fin class Cliente.

