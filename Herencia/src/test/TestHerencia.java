
package test;

import domain.Cliente;
import domain.Empleado; //importamos la clase Empleado del paquete domain.
import java.util.Date;

public class TestHerencia {
    public static void main(String[] args) {
        //Date fecha= new Date(122, 0, 2); //año init 1900 +122=2022, y mes init 0 entonces 0 =enero, dia es normal.
        
        Empleado emplado1= new Empleado("Juan", 5000.0); //utilizamos un constructor de la clase Empleado
        System.out.println("emplado1 = " + emplado1);
     
//        Cliente cliente1= new Cliente(fecha, true, "Shon", 'M', 25, "Jujuy, albear,355");
//        System.out.println("cliente1 = " + cliente1);
//        
//        Cliente cliente2= new Cliente(new Date(), false, "Maria", 'F', 35,"Purmamarca, chicot, 1955"); // newDate() es fecha actual
//        System.out.println("cliente1 = " + cliente2);
        
        
        
    }// fin main
    
} //fin class TestHerencia
